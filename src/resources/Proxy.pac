function FindProxyForURL(url, host) {
 
// If the hostname matches, send direct.
    if (dnsDomainIs(host, ".fw"))
        return 'PROXY 127.0.0.1:3344';
    return 'DIRECT';
 
}